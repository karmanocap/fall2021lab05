public class NumberedCards extends Card {
	private Numbers number;

    public NumberedCards(Colors color, Numbers number) {
        super(color);
        this.number = number;
    }
}
