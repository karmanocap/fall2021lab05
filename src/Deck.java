
import java.util.ArrayList;
import java.util.Random;

public class Deck {
	private ArrayList<Card> decks = new ArrayList<>();

	public Deck() {
		
		//Implement blue as a default color for now, not final though
		decks.add(new WildCard(Colors.BLUE));
		decks.add(new WildCard(Colors.BLUE));
		decks.add(new WildCard(Colors.BLUE));
		decks.add(new WildCard(Colors.BLUE));
		
		decks.add(new WildPickup4Card(Colors.BLUE));
		decks.add(new WildPickup4Card(Colors.BLUE));
		decks.add(new WildPickup4Card(Colors.BLUE));
		decks.add(new WildPickup4Card(Colors.BLUE));
		
		for (Colors color : Colors.values()) {
			for (Numbers number : Numbers.values()) {
				decks.add(new NumberedCards(color, number));
				decks.add(new NumberedCards(color, number));
				System.out.println(color);
			}
		}
		for (Colors color : Colors.values()) {
			
			decks.add(new PickUp2Card(color));
			decks.add(new PickUp2Card(color));
			
			decks.add(new ReverseCard(color));
			decks.add(new ReverseCard(color));
			
			decks.add(new SkipCard(color));
			decks.add(new SkipCard(color));
			System.out.println(color);
		}
	}
		
	public int getSize() {
		return decks.size();
	}

	public void deckShuffle() {

		Random rand = new Random();
		for (int i = 0; i < decks.size(); i++){
			int pos = rand.nextInt(decks.size() - 1);
			
			Card posCard = decks.get(pos);
			decks.set(pos, decks.get(i));
			decks.set(i, posCard);
		}
	}
	
	public void addToDeck(Card cards) {
		decks.add(cards);
	}
	
	public Card draw() {
		//removes first card and at the same time returns its value
		return decks.remove(0);
	}
}
