//@author Hamza Jaidi - 2031769
//Lab 05 Java III

public abstract class Card implements PlayCards {
   protected Colors color;

   protected Card(Colors color) {
      this.color = color;
   }

   public boolean canPlay() {
      return false;
   }
}
